package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {
    private Board board;

    // Oppretter et nytt brett før hver test.
    @BeforeEach
    void setUp() {
        board = new Board(10, 10);
    }

    // Sjekker om koordinatene er gyldige. Koordinatene (-1, 0), (0, -1), (10, 0) og (0, 10) er utenfor brettet,
    // og skal returnere false. Resten er gyldige koordinater.
    @Test
    void testIsValidCoordinate() {
        assertTrue(board.isValidCoordinate(0, 0));
        assertTrue(board.isValidCoordinate(9, 9));
        assertFalse(board.isValidCoordinate(-1, 0));
        assertFalse(board.isValidCoordinate(0, -1));
        assertFalse(board.isValidCoordinate(10, 0));
        assertFalse(board.isValidCoordinate(0, 10));
    }

    // Sjekker at cellene i hjørnene har riktig verdi. (0, 0) og (9, 9) skal være gyldige, mens (-1, 0), (0, -1),
    // (10, 0) og (0, 10) skal være null.
    @Test
    void testGetCell() {
        assertNotNull(board.getCell(0, 0));
        assertNotNull(board.getCell(9, 9));
        assertNull(board.getCell(-1, 0));
        assertNull(board.getCell(0, -1));
        assertNull(board.getCell(10, 0));
        assertNull(board.getCell(0, 10));
    }
}
