package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CellTest {
    private Cell cell;
    private Ship ship;

    // Oppretter en ny celle og et skip før hver test.
    @BeforeEach
    void setUp() {
        cell = new Cell();
        ship = new Ship("Battleship", 4, "/vertical_Battleship.png", "/horizontal_Battleship.png");
    }

    // Tester at en celle ikke er satt til å være truffet før den blir truffet.
    @Test
    void testIsHit() {
        assertFalse(cell.isHit());
    }

    // Tester at en celle blir satt til å være truffet når hit() kalles.
    @Test
    void testHit() {
        cell.hit();
        assertTrue(cell.isHit());
    }

    // Tester at en celle ikke har et skip før det blir plassert.
    @Test
    void testHasShip() {
        assertFalse(cell.hasShip());
    }

    // Tester at en celle har et skip etter at det blir plassert, at skipet er det samme som
    // ble kalt på, at skipets indeks stemmer med kallet og at skipets orientering stemmer med kallet.
    @Test
    void testSetShip() {
        cell.setShip(ship, 0, true);
        assertTrue(cell.hasShip());
        assertEquals(ship, cell.getShip());
        assertEquals(0, cell.getShipIndex());
        assertTrue(cell.getShipOrientation());
    }
}
