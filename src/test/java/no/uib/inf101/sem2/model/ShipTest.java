package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testklasse for Ship. Tester om navn passer det som blir hentet fra konstruktøren,
 * om størrelsen er riktig, om sunk blir riktig (itererer gjennom fire hits), 
 * og om bildene blir hentet riktig.
 */
class ShipTest {
    private Ship ship;

    // Oppretter et nytt skip som blir brukt i alle testene.
    @BeforeEach
    void setUp() {
        ship = new Ship("Battleship", 4, "/vertical_Battleship.png", "/horizontal_Battleship.png");
    }

    // Tester om navnet blir hentet på riktig måte.
    @Test
    void testGetName() {
        assertEquals("Battleship", ship.getName());
    }

    // Tester om størrelsen på skipet er satt riktig.
    @Test
    void testGetSize() {
        assertEquals(4, ship.getSize());
    }

    // Itererer gjennom fire hits og sjekker om sunk blir riktig for typen Battleship.
    @Test
    void testIsSunk() {
        assertFalse(ship.isSunk());
        
        ship.hit();
        assertFalse(ship.isSunk());
        
        ship.hit();
        assertFalse(ship.isSunk());

        ship.hit();
        assertFalse(ship.isSunk());
        
        ship.hit();
        assertTrue(ship.isSunk());
    }

    // Tester om bildene blir hentet riktig for både vertikal og horisontal plassering.
    @Test
    void testGetImages() {
        assertNotNull(ship.getVerticalImage());
        assertNotNull(ship.getHorizontalImage());
    }
}