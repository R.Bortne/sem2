package no.uib.inf101.sem2.model;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanPlayerTest {
    private HumanPlayer humanPlayer;
    private AIPlayer aiPlayer;
    private Ship ship;

    // Oppretter ny HumanPlayer, AIPlayer og Ship før hver test.
    @BeforeEach
    public void setUp() {
        humanPlayer = new HumanPlayer("Player", 10, 10);
        aiPlayer = new AIPlayer("AI", 10, 10);
        ship = new Ship("Battleship", 4, "/vertical_Battleship.png", "/horizontal_Battleship.png");
    }

    // Tester at spilleren kan skyte på en celle som er tom, og få tilbake false (miss).
    @Test
    public void testHandleShotMiss() {
        assertFalse(humanPlayer.handleShot(aiPlayer , 0, 0));
    }

    // Tester at spilleren kan skyte på en celle som har et skip, og få tilbake true (hit).
    @Test
    public void testHandleShotHit() {
        aiPlayer.getBoard().placeShip(ship, 0, 0, true);
        assertTrue(humanPlayer.handleShot(aiPlayer, 0, 0));
    }

    // Tester at du ikke kan skyte utenfor brettet fordi verdiene der vil væere null.
    @Test
    public void testHandleShotOutOfBounds() {
        assertThrows(NullPointerException.class, () -> humanPlayer.handleShot(aiPlayer, 10, 10));
    }
}