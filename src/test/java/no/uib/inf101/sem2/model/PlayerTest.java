package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private Player player;
    private Ship ship;

    // Oppretter en ny spiller og et skip før hver test.
    @BeforeEach
    void setUp() {
        player = new HumanPlayer("Test Player", 10, 10);
        ship = new Ship("Battleship", 4, "/vertical_Battleship.png", "/horizontal_Battleship.png");
    }

    // Plasserer et skip mes startpunkt på 0,0 og sjekker om det blir plassert riktig.
    // Prøver så å plassere et skip på samme plass og sjekker at det ikke går.
    @Test
    void testPlaceShip() {
        assertTrue(player.placeShip(ship, 0, 0, true));
        assertFalse(player.placeShip(ship, 0, 0, true));
    }

    // Skyter på skipet til det synker, og sjekker om hasLost() returnerer riktig.
    @Test
    void testHasLost() {
        // Må plassere et skip først for at hasLost() ikke blir true før vi begynner å skyte.
        player.placeShip(ship, 0, 0, true);
        assertFalse(player.hasLost());
        
        assertFalse(player.hasLost());
        
        ship.hit();
        assertFalse(player.hasLost());
        
        ship.hit();
        assertFalse(player.hasLost());

        ship.hit();
        assertFalse(player.hasLost());
        
        ship.hit();
        assertTrue(player.hasLost());
    }
}

