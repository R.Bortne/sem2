package no.uib.inf101.sem2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class AIPlayerTest {
    private AIPlayer aiPlayer;
    private HumanPlayer humanPlayer;

    // Lager en et skip, AIPlayer og en HumanPlayer som skal brukes i testene.
    @BeforeEach
    public void setUp() {
        aiPlayer = new AIPlayer("AI", 10, 10);
        humanPlayer = new HumanPlayer("Player", 10, 10);
    }

    // Tester at AIPlayer kan skyte på en celle hos HumanPlayer, og at hitCount øker med 1.
    // Bruker metoden i slutten av testen for å øke hitCount med 1 og returnerer ny hitCount.
    @Test
    public void testMakeRandomMove() {
        int initialHitCount = countHits(humanPlayer);
        aiPlayer.makeMove(humanPlayer);
        int finalHitCount = countHits(humanPlayer);

        assertTrue(finalHitCount > initialHitCount);
    }

    // Hjelpemetode for å telle hvor mange celler som er truffet.
    // Brukes i testMakeRandomMove(), returnerer hitCount.
    private int countHits(HumanPlayer player) {
        int hitCount = 0;
        for (int i = 0; i < player.getBoard().getNumRows(); i++) {
            for (int j = 0; j < player.getBoard().getNumCols(); j++) {
                if (player.getBoard().getCell(i, j).isHit()) {
                    hitCount++;
                }
            }
        }
        return hitCount;
    }

}
