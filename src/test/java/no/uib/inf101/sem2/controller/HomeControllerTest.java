package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;
import no.uib.inf101.sem2.view.GameView;
import no.uib.inf101.sem2.view.HomeView;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class HomeControllerTest {
    

    /**
     * Test at HomeView er synlig etter at HomeController er opprettet.
     */
    @Test
    void testHomeViewVisible() {
        HomeView homeView = new HomeView();
        HumanPlayer humanPlayer = new HumanPlayer("Player", 10, 10);
        AIPlayer aiPlayer = new AIPlayer("AI", 10, 10);
        new HomeController(homeView, humanPlayer, aiPlayer);

        assertTrue(homeView.isVisible());
    }

    /**
     * Tester at spillbrettet blir synlig etter at "Start Game" knappen i
     * hjem-skjermen er trykket på.
     */
    @Test
    void testGameViewVisibleAfterStartGame() {
        HomeView homeView = new HomeView();
        HumanPlayer humanPlayer = new HumanPlayer("Player", 10, 10);
        AIPlayer aiPlayer = new AIPlayer("AI", 10, 10);
        HomeController homeController = new HomeController(homeView, humanPlayer, aiPlayer);

        // Simulerer et klikk på Start Game knappen i Home-skjermen
        homeView.getStartButton().doClick();

        GameView gameView = homeController.getGameView();
        assertTrue(gameView.isVisible());
    }
}
