package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;
import no.uib.inf101.sem2.view.GameView;
import no.uib.inf101.sem2.view.HomeView;
import org.junit.jupiter.api.Test;


import java.awt.event.ActionEvent;

import static org.junit.jupiter.api.Assertions.*;

class GameControllerTest {

    // Test som sjekker at GameView blir synlig etter at GameController er opprettet.
    // Sjekker også at "BATTLE" knappen blir disabled når spillet har begynt.
    @Test
void testGameViewInitialization() {
    HomeView homeView = new HomeView();
    HumanPlayer humanPlayer = new HumanPlayer("Player", 10, 10);
    AIPlayer aiPlayer = new AIPlayer("AI", 10, 10);
    GameController gameController = new GameController(humanPlayer, aiPlayer, homeView);

    GameView gameView = gameController.getGameView();

    gameController.actionPerformed(new ActionEvent(gameView.getStartButton(), ActionEvent.ACTION_PERFORMED, null));

    assertFalse(gameView.getStartButton().isEnabled(), "StartButton should be disabled after game start");
    }

}
