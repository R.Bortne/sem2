package no.uib.inf101.sem2.model;

import java.awt.Point;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Random;

/**
 * En klasse som representerer AI-spilleren. AI-spilleren velger tilfeldige
 * celler å skyte på, men om den treffer et skip vil den fortsette å skyte på
 * tilstøtende celler til skipet er senket (i prinsippet i alle fall).
 * 
 * @author ChatGPT (denne klassen ble implementert i siste liten da jeg ikke fikk til en god nok
 * AI selv. Den opprinnelige og helt tilfeldigskytende AI-koden er likevel vedlagt nederst i klassen
 * og skrevet av meg.)
 */
public class AIPlayer extends Player {
    private final Random random;
    private final Deque<Point> targets;

    /**
     * Oppretter en ny AI-spiller med navn og brett med gitt størrelse.
     * 
     * @param name   Spillerens navn.
     * @param numRows   Antall rader på brettet.
     * @param numCols   Antall kolonner på brettet.
     */
    public AIPlayer(String name, int numRows, int numCols) {
        super(name, numRows, numCols);
        // Lager en ny tilfeldig generator.
        random = new Random();
        // Lager en ny tom kø til å lagre målene til AIPlayer.
        targets = new ArrayDeque<>();
    }

    public void makeMove(HumanPlayer opponent) {
        int row, col;

        // Sjekker om det er noen mål å køen.
        if (!targets.isEmpty()) {
            Point nextTarget = targets.pop();
            row = nextTarget.x;
            col = nextTarget.y;
        } else {
            // Om det ikke lenger er mål i køen skyter AIen på tilfeldige celler.
            do {
                row = random.nextInt(opponent.getBoard().getNumRows());
                col = random.nextInt(opponent.getBoard().getNumCols());
            } while (opponent.getBoard().getCell(row, col).isHit());
        }

        // Skyter på cellen. Bruker addAdjacentTargets() metoden for å skyte "smartere"
        // etter at et skip er truffet.
        Cell targetCell = opponent.getBoard().getCell(row, col);
        targetCell.hit();
        
        if (targetCell.hasShip()) {
            Ship hitShip = targetCell.getShip();
            hitShip.hit();

            if (!hitShip.isSunk()) {
                addAdjacentTargets(opponent.getBoard(), row, col);
            }
        }
    }
    // Metoden som legger til tilstøtende celler til målet i køen.
    private void addAdjacentTargets(Board opponentBoard, int row, int col) {
        int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    
        for (int[] direction : directions) {
            int newRow = row + direction[0];
            int newCol = col + direction[1];
    
            if (isValidAndUnhitCoordinate(opponentBoard, newRow, newCol)) {
                targets.push(new Point(newRow, newCol));
            }
        }
    }
    // Metoden som sjekker om cellen er gyldig og ikke er truffet allerede.
    private boolean isValidAndUnhitCoordinate(Board opponentBoard, int row, int col) {
        if (row >= 0 && row < opponentBoard.getNumRows() && col >= 0 && col < opponentBoard.getNumCols()) {
            Cell cell = opponentBoard.getCell(row, col);
            return !cell.isHit();
        }
        return false;
    }

/*   Denne koden er den opprinnelige AI-koden som bare skyter tilfeldig. Tenker å kanskje
     legge den til som en egen klasse for å kunne velge mellom flere vanskelighetsgrader en gang i fremtiden.
   
    public void makeMove(Player opponent) {
        int row;
        int col;
        Cell cell;

        do {
            row = random.nextInt(board.getNumRows());
            col = random.nextInt(board.getNumCols());
            cell = opponent.getBoard().getCell(row, col);
        } while (cell.isHit());

        cell.hit();

        if (cell.hasShip()) {
            Ship ship = cell.getShip();
            ship.hit();
        } */
    
}



