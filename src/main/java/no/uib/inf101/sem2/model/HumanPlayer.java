package no.uib.inf101.sem2.model;

/**
 * Representerer en menneskelig spiller i spillet.
 * Utvider Player klassen.
 */
public class HumanPlayer extends Player {

    /**
     * Oppretter en ny HumanPlayer med navn og brettstørrelse.
     * 
     * @param name   Spillerens navn.
     * @param numRows   Antall rader på brettet.
     * @param numCols  Antall kolonner på brettet.
     */
    public HumanPlayer(String name, int numRows, int numCols) {
        super(name, numRows, numCols);
    }

    /**
     * Metode for å håndtere et skudd fra HumanPlayer mot AIPlayer sitt brett.
     * Sjekker om en celle i mostanderbrettet har blitt truffet før, og hvis ikke
     * setter den cellen til å være truffet. Sjekker også om det er et skip i cellen,
     * og hvis det er et skip, setter den skipet til å være truffet.
     * 
     * @param opponent  Spilleren som blir skutt på.
     * @param row       Raden som blir skutt på.
     * @param col       Kolonnen som blir skutt på.
     * @return          true hvis det ble truffet et skip, false hvis ikke.
     */
    public boolean handleShot(Player opponent, int row, int col) {
        Cell cell = opponent.getBoard().getCell(row, col);
        boolean shipHit = false;

        if (!cell.isHit()) {
            cell.hit();

            if (cell.hasShip()) {
                Ship ship = cell.getShip();
                ship.hit();
                shipHit = true;
                
            }
        }

        return shipHit;
    }
    
}



