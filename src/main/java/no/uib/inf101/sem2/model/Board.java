package no.uib.inf101.sem2.model;

/**
 * En klasse som representerer et brett i spillet.
 */
public class Board {
    private final int numRows;
    private final int numCols;
    private final Cell[][] grid;

    /**
     * Oppretter et nytt brett med gitt antall rader og kolonner.
     * 
     * @param numRows   Antall rader på brettet.
     * @param numCols   Antall kolonner på brettet.      
     */
    public Board(int numRows, int numCols) {
        this.numRows = numRows;
        this.numCols = numCols;
        this.grid = new Cell[numRows][numCols];

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                grid[i][j] = new Cell();
            }
        }
    }

    /**
     * En metode som prøver å plassere et skip på den spesifiserte posisjonen. Returnerer true hvis
     * skipet ble plassert, false hvis ikke. Sjekker både horisontale og vertikale skip.
     * 
     * @param ship        Skipet som skal plasseres.
     * @param row           Raden skipet skal plasseres på.
     * @param col           Kolonnen skipet skal plasseres på.
     * @param isHorizontal  Om skipet skal plasseres horisontalt (true) eller vertikalt (false).
     * @return              true hvis skipet ble plassert, false hvis ikke.
     */
    public boolean placeShip(Ship ship, int row, int col, boolean isHorizontal) {
        if (isHorizontal) {
            if (col + ship.getSize() > numCols) {
                return false;
            }
            for (int i = col; i < col + ship.getSize(); i++) {
                if (grid[row][i].hasShip()) {
                    return false;
                }
            }
            for (int i = 0; i < ship.getSize(); i++) {
                grid[row][col + i].setShip(ship, i, isHorizontal);
            }
        } else {
            if (row + ship.getSize() > numRows) {
                return false;
            }
            for (int i = row; i < row + ship.getSize(); i++) {
                if (grid[i][col].hasShip()) {
                    return false;
                }
            }
            for (int i = 0; i < ship.getSize(); i++) {
                grid[row + i][col].setShip(ship, i, isHorizontal);
            }
        }
        return true;
    }

    /**
     * En metode som sjekker om et skip kan plasseres på den spesifiserte posisjonen. 
     * 
     * @param row       Raden som sjekkes.
     * @param col       Kolonnen som sjekkes.
     * @return          true hvis skipet kan plasseres, false hvis ikke.     
     */
    public boolean isValidCoordinate(int row, int col) {
        return row >= 0 && row < numRows && col >= 0 && col < numCols;
    }

    /**
     * Henter cellen som ligger på den spesifiserte posisjonen.
     * 
     * @param row   Raden til cellen.
     * @param col   Kolonnen til cellen.
     * @return      Cellen som ligger på den spesifiserte posisjonen om koordinatene
     *              er gyldige, null hvis ikke.
     */
    public Cell getCell(int row, int col) {
        return isValidCoordinate(row, col) ? grid[row][col] : null;
    }

    // Gettere for antall rader og kolonner.
    public int getNumRows() {
        return numRows;
    }

    public int getNumCols() {
        return numCols;
    }
}