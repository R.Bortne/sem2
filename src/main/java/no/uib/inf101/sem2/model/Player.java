package no.uib.inf101.sem2.model;

import java.util.ArrayList;
import java.util.List;


/**
 * En klasse til å representere spillerne i spillet.
 * Utvides av både AIPlayer og HumanPlayer.
 */
public abstract class Player {
    private final String name;
    protected final Board board;
    protected final List<Ship> ships;

    /**
     * Oppretter en ny spiller med navn og brett med gitt størrelse.
     * 
     * @param name      Spillerens navn.
     * @param numRows   Antall rader på brettet.
     * @param numCols   Antall kolonner på brettet.
     */
    public Player(String name, int numRows, int numCols) {
        this.name = name;
        this.board = new Board(numRows, numCols);
        // Lager en ny tom liste til å lagre skipene til spilleren.
        this.ships = new ArrayList<>();
    }

    /**
     * Legger til et skip til listen over skipene til spilleren.
     * 
     * @param ship  Skipet som skal legges til.
     */
    protected void addShip(Ship ship) {
        ships.add(ship);
    }

    /**
     * Bestemmer om et skip kan plasseres på brettet. Bruker Board sin metode
     * placeShip til å plassere skipet, og legger til skipet i listen over skipene
     * til spilleren om det ble plassert.
     * 
     * @param ship          Skipet som skal plasseres.
     * @param row           Raden skipet skal plasseres på.
     * @param col           Kolonnen skipet skal plasseres på.
     * @param isHorizontal  Om skipet skal plasseres horisontalt eller vertikalt.
     * @return              true hvis skipet ble plassert, false hvis ikke.
     */
    public boolean placeShip(Ship ship, int row, int col, boolean isHorizontal) {
        if (board.placeShip(ship, row, col, isHorizontal)) {
            addShip(ship);
            return true;
        }
        return false;
    }

    /**
     * Sjekker om spilleren har tapt (dvs. alle skipene er sunket).
     * 
     * @return true hvis spilleren har tapt, false hvis ikke.
     */
    public boolean hasLost() {
        for (Ship ship : ships) {
            if (!ship.isSunk()) {
                return false;
            }
        }
        return true;
    }

    // Gettere for navn og brett.
    public String getName() {
        return name;
    }

    public Board getBoard() {
        return board;
    }

}

