package no.uib.inf101.sem2.model;

/**
 * Klasse for å representere en celle på spillbrettet. 
 */
public class Cell {
    private boolean hit;
    private Ship ship;
    private int shipIndex;
    private boolean shipOrientation;
    

    /**
     * Metode som setter skipet, skipets indeks og skipets orientasjon for denne cellen.
     * 
     * @param ship           Skipet som okkuperer denne cellen.
     * @param shipIndex      Indeks til cellene i skipet (eksempelvis 0, 1, 2, 3, 4 for Battleship).
     * @param shipOrientation   true hvis skipet er vertikalt, false hvis skipet er horisontalt.
     */
    public void setShip(Ship ship, int shipIndex, boolean shipOrientation) {
        this.ship = ship;
        this.shipIndex = shipIndex;
        this.shipOrientation = shipOrientation;
    }

    /**
     * Konstruktør for Cell. Lager en ny celle uten skip.
     */
    public Cell() {
        this.ship = null;
    }

    /**
     * Metode som sjekker om cellen er truffet.
     * 
     * @return true hvis cellen er truffet, false hvis ikke.
     */
    public boolean isHit() {
        return hit;
    }

    /**
     * Metode som setter cellen til truffet.
     */
    public void hit() {
        this.hit = true;
    }

    /**
     * Metode som sjekker om cellen har et skip.
     * 
     * @return true hvis cellen har et skip, false hvis ikke.
     */
    public boolean hasShip() {
        return ship != null;
    }

    /**
     * Gettere for skipet, skipets index og skipets orientasjon.
     */
    public Ship getShip() {
        return ship;
    }

    public int getShipIndex() {
        return shipIndex;
    }

    public boolean getShipOrientation() {
        return shipOrientation;
    }
    
}

