package no.uib.inf101.sem2.model;

import javax.swing.ImageIcon;

/** 
 * En klasse som er ansvarlig for å lage skipene i spillet. 
*/
public class Ship {
    private final String name;
    private final int size;
    private int hits;
    private final ImageIcon verticalImage;
    private final ImageIcon horizontalImage;

    /**
     * Konstruktør for Ship med navn, størrelse og bilde for vertikal- og horisontal posisjon.
     * 
     * @param name                  Skipets navn.
     * @param size                  Skipets størrelse.
     * @param verticalImagePath     Path til bilde for vertikal posisjon.
     * @param horizontalImagePath   Path til bilde for horisontal posisjon.
     */
    public Ship(String name, int size, String verticalImagePath, String horizontalImagePath) {
        this.name = name;
        this.size = size;
        this.hits = 0;
        this.verticalImage = new ImageIcon(getClass().getResource(verticalImagePath));
        this.horizontalImage = new ImageIcon(getClass().getResource(horizontalImagePath));
    }

    /**
     * Metode som sjekker om skipet er sunket ved å se om alle cellene til skipet er truffet.
     * 
     * @return  true hvis skipet er sunket, false hvis ikke.
     */
    public boolean isSunk() {
        return hits >= size;
    }

    /**
     * Metode som øker antall treff på skipet med 1.
     */
    public void hit() {
        if (!isSunk()) {
            hits++;
        }
    }

    /**
     * Gettere og settere for skipets navn, størrelse og bilder for bruk til vertikal- og horisontal posisjon.
     */
    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public ImageIcon getVerticalImage() {
        return verticalImage;
    }

    public ImageIcon getHorizontalImage() {
        return horizontalImage;
    }
}



