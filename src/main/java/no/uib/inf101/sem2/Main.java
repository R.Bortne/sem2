package no.uib.inf101.sem2;

import javax.swing.SwingUtilities;

import no.uib.inf101.sem2.controller.HomeController;
import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;
import no.uib.inf101.sem2.util.ShipStarter;
import no.uib.inf101.sem2.view.HomeView;

/**
 * Main klassen for BattleShip spillet. Starter spillet ved å opprette en ny
 * HomeView, før den lager to nye spillere, og plasserer skipene til begge
 * to. Deretter opprettes en ny HomeController.
 * 
 * @author Roger Bortne
 */
public class Main {
    public static void main(String[] args) {
        // Forsikrer at UI oppdateres på riktig måte på EDT tråden.
        SwingUtilities.invokeLater(() -> {
            // Oppretter en ny HomeView, og to nye spillere.
            HomeView homeView = new HomeView();
            HumanPlayer humanPlayer = new HumanPlayer("Player", 10, 10);
            AIPlayer aiPlayer = new AIPlayer("AI", 10, 10);

            // Plasserer skipene til spillerne.
            ShipStarter.startShips(humanPlayer);
            ShipStarter.startShips(aiPlayer);

            // Oppretter en ny HomeController med spillerne. 
            new HomeController(homeView, humanPlayer, aiPlayer);
        });
    }
}


