package no.uib.inf101.sem2.util;

import no.uib.inf101.sem2.model.Player;
import no.uib.inf101.sem2.model.Ship;

import java.util.List;

/**
 * En hjelpeklasse for å plassere skipene til spillerne på brettet.
 * Brukes i Main, og kunne fint ha stått der egentlig, men å ha en egen hjelpekasse gjør det mer ryddig.
 * 
 * @author Roger Bortne
 */
public class ShipStarter {

    /**
     * Plasserer skip tilfeldig på brettet. Bruker ShipPlacer klassen til å plassere skipene.
     * 
     * @param player    Spilleren hvis skip skal plasseres
     */
    public static void startShips(Player player) {
        // Henter ut listen med skip fra ShipTypes klassen som skal plasseres.
        List<Ship> ships = ShipTypes.getDefaultShips();
        // Kaller på metoden i ShipPlacer klassen som plasserer skipene tilfeldig.
        ShipPlacer.placeShipsRandomly(player, ships); 
    }
}


