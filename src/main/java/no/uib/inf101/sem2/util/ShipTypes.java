package no.uib.inf101.sem2.util;

import no.uib.inf101.sem2.model.Ship;

import java.util.Arrays;
import java.util.List;

/**
 * En hjelpeklasse med metode for å lage en liste med standard skip for spillet.
 * Returnerer en liste med skip som inneholder navn på skipet, størrelsen på skipet og plasseringen til bilder for vertikal- og horisontal orientering.
 */
public class ShipTypes {
    public static List<Ship> getDefaultShips() {
        return Arrays.asList(
                new Ship("Carrier", 5, "/vertical_Carrier.png", "/horizontal_Carrier.png"),
                new Ship("Battleship", 4, "/vertical_Battleship.png", "/horizontal_Battleship.png"),
                new Ship("Cruiser", 3, "/vertical_Cruiser.png", "/horizontal_Cruiser.png"),
                new Ship("Submarine", 3, "/vertical_Submarine.png", "/horizontal_Submarine.png"),
                new Ship("Destroyer", 2, "/vertical_Destroyer.png", "/horizontal_Destroyer.png")
        );
    }
}


