package no.uib.inf101.sem2.util;

import java.io.InputStream;
import javax.sound.midi.*;

/**
 * En klasse for å spille av MIDI-filer i spillet. Den henter MIDI-filer fra resources-mappen,
 * og spiller av en fil om gangen. Den kan også settes til å spille av en fil i loop (noe som er
 * gjort her).
 * 
 * @author Roger Bortne
 */
public class MusicPlayer {
    private Sequencer sequencer;
    private Sequence sequence;
    private boolean loop;

    /**
     * Konstruktør for MusicPlayer klassen. Den setter opp sequencer og legger til en
     * MetaEventListener som sjekker om en fil er ferdig spilt av. Om en fil er ferdig spilt av,
     * starter den på nytt om loop er satt til true.
     */
    public MusicPlayer() {
        try {
            // Henter en sequencer og åpner den (en sequencer er en MIDI avspiller).
            sequencer = MidiSystem.getSequencer();
            sequencer.open();
            // Henter en MetaEventListener som sjekker om en fil er ferdig spilt av (om beskjeden er 47).
            sequencer.addMetaEventListener(new MetaEventListener() {
                public void meta(MetaMessage msg) {
                    // Om beskjeden er 47 og loop er satt til true, starter sangen på nytt.
                    if (msg.getType() == 47 && loop) {
                        sequencer.setTickPosition(0);
                        sequencer.start();
                    }
                }
            });
        // Håndterer feil som om sequencer ikke kan hentes eller MIDI filen ikke finnes eller er korrupt etc.    
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metode for å hente og spille av en spesifikk MIDI fil.
     * 
     * @param filename  Filnavnet til MIDI filen som skal spilles av.
     */
    public void play(String filename) {
        try {
            // Laster MIDI filen fra resources-mappen. Dette er mulig gjennom å bruke en ClassLoader som
            // gir metoden tilgang til resources-mappen.
            ClassLoader classLoader = getClass().getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream(filename);

            // Lager en sequence fra MIDI filen i InputStream.
            sequence = MidiSystem.getSequence(inputStream);

            // Setter sequencer til å spille av sequence (sangen).
            sequencer.setSequence(sequence);
            sequencer.start();

        // Håndterer feil som skjer ved avspilling av MIDI filen.    
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metode for å stoppe avspillingen av en MIDI fil.
     */
    public void stop() {
        sequencer.stop();
        sequencer.setTickPosition(0);
    }

    // Gettere og settere for loop og loopCount.
    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public void setLoopCount(int count) {
        sequencer.setLoopCount(count);
    }

    public void close() {
        sequencer.close();
    }
}

