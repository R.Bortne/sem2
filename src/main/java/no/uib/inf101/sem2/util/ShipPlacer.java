package no.uib.inf101.sem2.util;


import no.uib.inf101.sem2.model.Player;
import no.uib.inf101.sem2.model.Ship;

import java.util.List;
import java.util.Random;

/**
 * En hjelpeklasse for å plassere skip tilfeldig rundt på brettet.
 * 
 * @author Roger Bortne
 */
public class ShipPlacer {
    private static final Random random = new Random();

    /**
     * Plasserer skipene tilfeldig rundt på brettet.
     * Kaller på metoden placeShip i Player klassen, og prøver å plassere skipet tilfeldig 
     * inntil det lykkes.
     *
     * @param player spilleren som skal plassere skipene
     * @param ships  listen med skip som skal plasseres
     */
    public static void placeShipsRandomly(Player player, List<Ship> ships) {
        for (Ship ship : ships) {
            boolean placed = false;
            while (!placed) {
                int row = random.nextInt(player.getBoard().getNumRows());
                int col = random.nextInt(player.getBoard().getNumCols());
                boolean isHorizontal = random.nextBoolean();
                placed = player.placeShip(ship, row, col, isHorizontal);
            }
        }
    }
}

