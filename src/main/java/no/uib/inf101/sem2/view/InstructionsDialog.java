package no.uib.inf101.sem2.view;

import javax.swing.*;
import java.awt.*;

/**
 * En klasse som tar en JDialog for å vise instruksjoner for spillet fra Hjem-skjermen.
 * Inneholder en JTextArea tekstboks med instruksjoner for spillet.
 * 
 * @author Roger Bortne
 */
public class InstructionsDialog extends JDialog {

    /**
     * Konstruktør for InstructionsDialog, som tar en JFrame som eier.
     * At JFrame er "owner" betyr at InstructionsDialog vil være på toppen av JFrame
     * som lager Hjem-vinduet, helt til vi velger å lukke InstructionsDialog.
     * 
     * @param owner JFrame som eier InstructionsDialog.
     */
    public InstructionsDialog(JFrame owner) {
        super(owner, "Instructions", true);

        // Lager en JTextArea tekstboks med instruksjoner for spillet.
        JTextArea instructionsTextArea = new JTextArea();
        instructionsTextArea.setEditable(false);
        instructionsTextArea.setLineWrap(true);
        instructionsTextArea.setWrapStyleWord(true);
        instructionsTextArea.setFont(new Font("Arial", Font.BOLD, 16));
        instructionsTextArea.setText(
                "Instructions:\n\n" +
                        "1. Click 'Start Game' on the home screen to begin.\n\n" +
                        "2. The game will start with two grids. The left grid is your board, and the right grid is the AI's board.\n\n" +
                        "3. Your ships are pre-placed on your board.\n\n" +
                        "4. Click on the AI's board to attack a cell. The AI will respond with an attack on your board.\n\n" +
                        "5. The game continues until all ships for one player are sunk. A player loses when all their ships are sunk.\n\n" +
                        "6. Unfortunately, the only way to play the game again in this version is to press 'Quit' and start the app once more.\n\n" +
                        "7. Lykke til skipper!"
        );

        // Setter bakgrunnsfarge og tekstfarge for JTextArea.
        instructionsTextArea.setBackground(new Color(0, 116, 162, 255));
        instructionsTextArea.setForeground(new Color(199, 213, 224, 255));

        // Legger JTextArea inn i en JScrollPane, slik at vi får en scrollbar hvis teksten er for lang (noe den ikke er,
        // men det er en god vane å gjøre dette likevel)
        JScrollPane scrollPane = new JScrollPane(instructionsTextArea);
        scrollPane.setBorder(null);
        scrollPane.setPreferredSize(new Dimension(450, 450));
        scrollPane.getViewport().setBackground(new Color(0, 116, 162, 255));

        // Lager en JButton for å lukke InstructionsDialog.
        JButton closeButton = new JButton("Close");
        closeButton.setBackground(new Color(199, 213, 224, 200));
        closeButton.setForeground(new Color(248, 56, 0, 255));
        closeButton.setFont(new Font("Arial", Font.BOLD, 20));
        closeButton.setFocusable(false);
        closeButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        closeButton.addActionListener(e -> dispose());

        // Lager en JPanel for å legge closeButton inn i.
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(closeButton);

        // Legger scrollPane og buttonPanel inn i InstructionsDialog.
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(owner);
    }
}

