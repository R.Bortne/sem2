package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.Cell;

import javax.swing.*;
import java.awt.*;

/**
 * Klasse for å tegne selve cellene på spillbrettene. Klassen lager farger for hver celle
 * og tegner disse fargene på cellene avhengig av celles status (sjø, treffer, bom, sunket skip, osv.).
 * paintComponent-metoden er ansvarlig for å hente og justere .png bildene av skipene og å placere disse 
 * bildene i cellene.
 *
 * @author ChatGPT og Roger Bortne
 */
public class CellView extends JPanel {
    private static final Color HIT_COLOR = new Color (248, 56, 0, 255); 
    private static final Color MISS_COLOR = new Color(199, 213, 224, 255);
    private static final Color SEA_COLOR = new Color(0, 116, 162, 255);
    private static final Color SUNK_COLOR = new Color(110, 5, 17, 255);
    private final Cell cell;
    private boolean isHidden;

    /**
     * Konstruktør for CellView. Setter farge på grid mellom cellene samt størrelsen på cellene.
     */
    public CellView(Cell cell) {
        this.cell = cell;
        this.isHidden = false;
        setBorder(BorderFactory.createLineBorder(Color.WHITE));
        setPreferredSize(new Dimension(60, 60));
    }

    /**
     * Metode for å skjule cellene i brettet. Brukes for å skjule AIPlayer sine skip.
     * Repaint() kalles for å oppdatere cellene slik at cellene som er skjulte ikke forblir det
     * om f.eks et skip blir truffet.
     * 
     * @param isHidden  true hvis cellene skal skjules, false hvis ikke.
     */
    public void setHidden(boolean isHidden) {
        this.isHidden = isHidden;
        repaint();
    }

    /**
     * Denne metoden maler .png bilder av skipene over cellene i brettet og farger cellene avhengig av status.
     * Dette var ikke en enkel sak å få til på en god nok måte, og jeg skal ikke ta noe mer av æren for denne koden
     * enn at jeg har brukt mye tid på å forklare ChatGPT hva jeg ønsker å få ut av den. 
     * Opprinnelig hadde jeg kode for å bare vise farger, noe som fungerte fint, men jeg ønsket å få til noe mer.
     * 
     * @author ChatGPT
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        // Fargelegger cellene avhengig av status.
        g.setColor(SEA_COLOR);
        g.fillRect(0, 0, getWidth(), getHeight());

        if (cell.isHit()) {
            if (cell.hasShip()) {
                g.setColor(HIT_COLOR);
            } else {
                g.setColor(MISS_COLOR);
            }
            g.fillRect(0, 0, getWidth(), getHeight());
        } else if (!isHidden && cell.hasShip()) {
            ImageIcon shipImage;
            Image scaledImage;
            int shipSize = cell.getShip().getSize();

            if (cell.getShipOrientation()) {
                // Om skipet er horisontalt hentes et horisontalt bilde av skipet.
                // Skalerer bildet til å passe over cellene som tilsvarer skipets størrelse.
                shipImage = cell.getShip().getHorizontalImage();
                scaledImage = shipImage.getImage().getScaledInstance((int) (getWidth() * shipSize * 0.85), (int) (getHeight() * 0.85), Image.SCALE_SMOOTH);
                new ImageIcon(scaledImage);
                g.drawImage(scaledImage, -(cell.getShipIndex() * getWidth()), (int) ((1 - 0.85) * getHeight() / 2), (int) (getWidth() * shipSize * 0.85), (int) (getHeight() * 0.85), null);
            } else {
                // Om skipet er vertikalt hentes et vertikalt bilde av skipet.
                // Skalerer bildet til å passe over cellene som tilsvarer skipets størrelse.
                shipImage = cell.getShip().getVerticalImage();
                scaledImage = shipImage.getImage().getScaledInstance((int) (getWidth() * 0.85), (int) (getHeight() * shipSize * 0.85), Image.SCALE_SMOOTH);
                new ImageIcon(scaledImage);
                g.drawImage(scaledImage, (int) ((1 - 0.85) * getWidth() / 2), -(cell.getShipIndex() * getHeight()), (int) (getWidth() * 0.85), (int) (getHeight() * shipSize * 0.85), null);
            }
        }

        // Sjekker om skipet er sunket, og fyller cellen med mørkerød farge hvis det er tilfelle.
        if (cell.hasShip() && cell.getShip().isSunk()) {
            g.setColor(SUNK_COLOR);
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }
}




