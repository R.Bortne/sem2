package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.util.MusicPlayer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

/**
 * HomeView er en klasse som lager et JPanel som viser Home/Hjem-vinduet til spillet.
 * Klassen setter et bilde som bakgrunn, og har knapper for å starte spillet, se instruksjoner og avslutte.
 * Klassen spiller også musikk når vinduet åpnes.
 * 
 * @author  Roger Bortne
 */
public class HomeView extends JPanel {
    private final JButton startButton = new JButton("Start Game");
    private final JButton instructionsButton = new JButton("Instructions");
    private final JButton exitButton = new JButton("Exit");
    private final BufferedImage backgroundImage;
    private final JFrame frame;
    private final MusicPlayer musicPlayer = new MusicPlayer();

    public HomeView() {
        // Bruker InputStream for å lese bildefilen "Background.png" fra resources-mappen.
        InputStream imageStream = getClass().getResourceAsStream("/Background.png");
        BufferedImage loadedImage = null;
        // Prøver å laste inn bildefilen og legge den i loadedImage.
        // Lager et BufferedImage fra InputStream. I tilfelle det ikke går, skrives feilmeldingen til konsollen.
        try {
            loadedImage = ImageIO.read(imageStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        backgroundImage = loadedImage;

        // Lager et JFrame for å vise HomeView.
        frame = new JFrame("Battleship - Home");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        // Setter en layout for panelet. Y_AXIS betyr at panelet skal ha en vertikal layout,
        // og at knappene skal settes under hverandre.
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        // Kaller på "styleButton" og "addHoverEffect" for å style knappene.
        styleButton(startButton);
        addHoverEffect(startButton); 
        styleButton(instructionsButton);
        addHoverEffect(instructionsButton);
        styleButton(exitButton);
        addHoverEffect(exitButton);

        // Setter en vertikal avstand mellom knappene.
        add(Box.createVerticalStrut(50)); 
        add(startButton);
        add(Box.createVerticalStrut(20)); 
        add(instructionsButton);
        add(Box.createVerticalStrut(20)); 
        add(exitButton);

        // Setter knappene til venstrejustert.
        startButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        instructionsButton.setAlignmentX(Component.LEFT_ALIGNMENT);
        exitButton.setAlignmentX(Component.LEFT_ALIGNMENT);

        // Setter en ramme rundt panelet for å få en litt bedre plassering av knappene.
        setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30)); 

        // Setter størrelsen på panelet til størrelsen på bildet som blir lastet.
        setPreferredSize(new Dimension(backgroundImage.getWidth(), backgroundImage.getHeight())); 
        frame.setContentPane(this);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);

        // Starter musikken i HomeWindow. -1 betyr at den skal spille på repeat.
        musicPlayer.play("TOPGUN.mid");
        musicPlayer.setLoopCount(-1);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Tegner bakgrunnsbildet på panelet ved hjelp av BufferedImage.
        g.drawImage(backgroundImage, 0, 0, this);
    }

    // Metode som legger til en hover-effekt på knappene.
    // Fungerer så som så i HomeWindow. Endrer utseende på knappene når musen er over dem.
    private void addHoverEffect(JButton button) {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                button.setBackground(new Color (0, 116, 162, 255));
                button.setForeground(new Color(199, 213, 224, 255));
            }
    
            @Override
            public void mouseExited(MouseEvent e) {
                button.setBackground(new Color(199, 213, 224, 255));
                button.setForeground(new Color (0, 116, 162, 255));
            }
        });
    }

    // Metode brukt av GameController som stopper musikken når vi starter BATTLE i selve spillet.
    public void stopMusic() {
        musicPlayer.stop();
    }

    // Metode som setter bakgrunnsfarge, tekstfarge, font, fokus og cursor på knappene.
    private void styleButton(JButton button) {
        button.setBackground(new Color(199, 213, 224, 200)); 
        button.setForeground(new Color(0, 116, 162, 255));
        button.setFont(new Font("Arial", Font.BOLD, 25)); 
        button.setFocusable(false);
        button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    // Metode som brukes for å fjerne JFrame til HomeWindow når vi går over i selve spillet.
    // Uten denne metoden blir vinduet liggende åpent i bakgrunnen. 
    public void dispose() {
        frame.dispose();
    }
    
    // Gettere for å kunne hente knappene i HomeController.
    public JButton getStartButton() {
        return startButton;
    }

    public JButton getInstructionsButton() {
        return instructionsButton;
    }

    public JButton getExitButton() {
        return exitButton;
    }

    public JFrame getFrame() {
        return frame;
    }
    
}




