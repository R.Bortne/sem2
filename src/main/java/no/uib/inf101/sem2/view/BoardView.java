package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.Board;

import javax.swing.*;
import java.awt.*;

/**
 * En klasse for å få en grafisk representasjon av spillebrettet.
 * Lager en grid av CellView objekter som representerer hver celle i spillebrettet.
 * 
 * @author Roger Bortne
 */
public class BoardView extends JPanel {
    private final int numRows;
    private final int numCols;
    private final CellView[][] cellViews;

    /**
     * Konstruktør for BoardView som lager en grid av CellView objekter som til sammen utgjør både AIPlayer og
     * HumanPlayer sine spillebrett. Setter AIPlayer-brettet til å være skjult (true).
     * 
     * @param board Spillebrettet som skal vises
     * @param isAIPlayer Om spilleren er AIPlayer eller ikke
     */
    public BoardView(Board board, boolean isAIPlayer) {
        super();
        this.numRows = board.getNumRows();
        this.numCols = board.getNumCols();
        this.cellViews = new CellView[numRows][numCols];
        setLayout(new GridLayout(numRows, numCols));
    
        // For-løkke som legger CellView objekter til i BoardView griden.
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                CellView cellView = new CellView(board.getCell(i, j));
                if (isAIPlayer) {
                    cellView.setHidden(true);
                }
                cellViews[i][j] = cellView;
                add(cellView);
            }
        }
    }
    
    /**
     * Metode for å hente ut en CellView fra BoardView griden.
     * 
     * @param row   Raden til CellView objektet som skal hentes.
     * @param col   Kolonnen til CellView objektet som skal hentes.
     * @return      CellView objektet som ble hentet fra raden og kolonnen.
     */
    public CellView getCellView(int row, int col) {
        return cellViews[row][col];
    }

    /**
     * Oppdaterer brettene ved å repainte alle CellView objektene.
     */
    public void refresh() {
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                cellViews[i][j].repaint();
            }
        }
    }
    
    /**
     * Oppdaterer en enkelt celle ved å repainte CellView objektet.
     * 
     * @param row   Raden til CellView objektet som skal oppdateres.
     * @param col   Kolonnen til CellView objektet som skal oppdateres.
     */
    public void refreshCell(int row, int col) {
        cellViews[row][col].repaint();
    }
}

