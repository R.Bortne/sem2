package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Klasse for å vise selve spillbrettet. Inneholder en BoardView for hver spiller, samt en JPanel
 * for å vise JButton knappene midt mellom brettene.
 * 
 * @author Roger Bortne
 */
public class GameView extends JFrame {
    private final BoardView humanBoardView;
    private final BoardView aiBoardView;
    private final JButton startButton = new JButton("BATTLE");
    private final JButton quitButton = new JButton("Quit");

    public GameView(HumanPlayer humanPlayer, AIPlayer aiPlayer) {
        setTitle("Battleship");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Tegner brettene for HumanPlayer og AIPlayer. Setter AIPlayer-brettet til å være skjult (true).
        humanBoardView = new BoardView(humanPlayer.getBoard(), false);
        aiBoardView = new BoardView(aiPlayer.getBoard(), true);

        // Konfigurering av Start (BATTLE) og Quit knappene. Legger til hover-effekt og setter farge og font.
        startButton.setBackground(new Color(199, 213, 224, 255));
        startButton.setForeground(new Color (248, 56, 0, 255));
        startButton.setFont(new Font("Arial", Font.BOLD, 14));
        startButton.setFocusable(false);
        addHoverEffect(startButton);

        quitButton.setBackground(new Color(199, 213, 224, 255));
        quitButton.setForeground(new Color (248, 56, 0, 255));
        quitButton.setFont(new Font("Arial", Font.BOLD, 14));
        quitButton.setFocusable(false);
        addHoverEffect(quitButton);

        // Konfigurerer knappene i GameView-brettet. Legger til en BoxLayout og en vertikal 
        // glue for å få knappene til å bli plassert under hverandre. Setter x-aksen til å være midtstilt.
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        buttonPanel.setPreferredSize(new Dimension(200, 600));
        buttonPanel.add(Box.createVerticalGlue());

        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonPanel.add(startButton);
        buttonPanel.add(Box.createVerticalStrut(10));

        quitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        buttonPanel.add(quitButton);

        buttonPanel.add(Box.createVerticalGlue());

        JPanel centerPanel = new JPanel();
        centerPanel.setBackground(Color.WHITE);
        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(buttonPanel, BorderLayout.CENTER);

        // Legger til brettene og knappene i GameView, henholdsvis i øst (høyre), vest (venstre) og midten.
        add(humanBoardView, BorderLayout.WEST);
        add(aiBoardView, BorderLayout.EAST);
        add(centerPanel, BorderLayout.CENTER);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);

        // Setter en krysspeker som musepeker når det siktes på AI-brettet.
        aiBoardView.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    }

    // Metode for å legge til hover-effekt på knappene i GameView-brettet ved å bruke MouseEvents.
    // Metoden forandrer bakgrunnsfarge og tekstfarge når musen er over knappen og forandrer tilbake til normalt når musen forlater knappen.
    private void addHoverEffect(JButton button) {
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                button.setBackground(new Color (248, 56, 0, 255));
                button.setForeground(new Color(199, 213, 224, 255));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                button.setBackground(new Color(199, 213, 224, 255));
                button.setForeground(new Color (248, 56, 0, 255));
            }
        });
    }

    // Getter metoder for knappene og brettene på GameView-brettet
    public BoardView getHumanBoardView() {
        return humanBoardView;
    }

    public BoardView getAIBoardView() {
        return aiBoardView;
    }

    public JButton getStartButton() {
        return startButton;
    }

    public JButton getQuitButton() {
        return quitButton;
    }
}


