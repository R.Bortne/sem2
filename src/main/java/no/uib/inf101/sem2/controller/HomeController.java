package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;
import no.uib.inf101.sem2.view.GameView;
import no.uib.inf101.sem2.view.HomeView;
import no.uib.inf101.sem2.view.InstructionsDialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * En klasse som håndterer det som skjer i Home vinduet. Håndterer også
 * overgangen fra Home til Game vinduet. AIPlayer har ikke sitt eget Home-vindu, 
 * men blir laget i GameController og det er derfor greit å ha en AIPlayer som kan
 * sendes med til GameController. Tok litt tid å finne ut av det ja :)
 * 
 * @author Roger Bortne
 */
public class HomeController {
    private final HomeView homeView;
    private final HumanPlayer humanPlayer;
    private final AIPlayer aiPlayer;
    private GameView gameView;

    /**
     * Konstruktøren til HomeController.
     * 
     * @param homeView    HomeView objektet som skal håndteres.
     * @param humanPlayer   HumanPlayer objektet som spiller spillet.
     * @param aiPlayer    AIPlayer objektet som spiller spillet, selv om det ikke er et eget Home-vindu for AIPlayer.
     */
    public HomeController(HomeView homeView, HumanPlayer humanPlayer, AIPlayer aiPlayer) {
        this.homeView = homeView;
        this.humanPlayer = humanPlayer;
        this.aiPlayer = aiPlayer;

        this.homeView.getStartButton().addActionListener(new StartGameButtonListener());
        this.homeView.getInstructionsButton().addActionListener(e -> showInstructions());
        this.homeView.getExitButton().addActionListener(e -> System.exit(0));
    }

    /**
     * En privat klasse som håndterer overgangen fra HomeView til GameView.
     * Har en actionListener som starter GameController når "Start Game" knappen trykkes.
     * Kaller også dispose() på HomeView for å fjerne den fra minnet og lukke vinduet.
     */
    private class StartGameButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            homeView.setVisible(false);
            homeView.dispose();
            GameController gameController = new GameController(humanPlayer, aiPlayer, homeView);
            gameView = gameController.getGameView();
            gameView.setVisible(true);
        }
    }

    /**
     * Metode for å vise instruksjonene til spillet i et nytt vindu om "Instructions" 
     * knappen trykkes.
     */
    private void showInstructions() {
        InstructionsDialog instructionsDialog = new InstructionsDialog(homeView.getFrame());
        instructionsDialog.setVisible(true);
    }

    public GameView getGameView() {
        return gameView;
    }
    
}
