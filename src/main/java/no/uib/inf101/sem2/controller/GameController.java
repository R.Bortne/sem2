package no.uib.inf101.sem2.controller;

import javax.swing.*;

import no.uib.inf101.sem2.model.AIPlayer;
import no.uib.inf101.sem2.model.HumanPlayer;
import no.uib.inf101.sem2.util.MusicPlayer;
import no.uib.inf101.sem2.view.GameView;
import no.uib.inf101.sem2.view.HomeView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Kontroller for spillet. Tar seg av å starte spillet, og håndtere klikk på brettene.
 * Kontrolleren sjekker også om spillet er ferdig, og viser en melding til brukeren.
 * 
 * @author Roger Bortne
 */
public class GameController implements ActionListener {
    private final GameView gameView;
    private final HumanPlayer humanPlayer;
    private final AIPlayer aiPlayer;
    private final HomeView homeView;
    private boolean gameStarted = false;
    private final MusicPlayer musicPlayer = new MusicPlayer();

    /**
     * Konstruktør for GameController. Lager en ny GameView, og legger til en
     * actionListener på "BATTLE"-knappen. Legger også til en actionListener på
     * quit-knappen som avslutter programmet. Legger til en MouseListener på
     * hver celle i AI-spillerens brett.
     * 
     * @param humanPlayer   HumanPlayer objektet som spiller spillet.
     * @param aiPlayer      AIPlayer objektet som spiller mot HumanPlayer.
     * @param homeView      HomeView objektet som ble brukt for å starte spillet.
     */
    public GameController(HumanPlayer humanPlayer, AIPlayer aiPlayer, HomeView homeView) {
        this.humanPlayer = humanPlayer;
        this.aiPlayer = aiPlayer;
        this.homeView = homeView;
        this.gameView = new GameView(humanPlayer, aiPlayer);
        this.gameView.getStartButton().addActionListener(this);
        this.gameView.getQuitButton().addActionListener(e -> System.exit(0));

        // Legger til en MouseListener på hver celle i AI-spillerens brett.
        for (int i = 0; i < aiPlayer.getBoard().getNumRows(); i++) {
            for (int j = 0; j < aiPlayer.getBoard().getNumCols(); j++) {
                gameView.getAIBoardView().getCellView(i, j).addMouseListener(new CellClickListener(i, j));
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == gameView.getStartButton()) {
            homeView.stopMusic();
            gameStarted = true;
            gameView.getStartButton().setEnabled(false);

            // Starter en egen sang for selve spillet. Erstatter sangen fra Home-vinduet.
            musicPlayer.play("hanger.mid");
            musicPlayer.setLoopCount(-1);
        }
    }

    /**
     * Sjekker om spillet er ferdig. Hvis spillet er ferdig, vises en melding til
     * brukeren og musikken stoppes. Reaktiverer også "BATTLE"-knappen, men her mangler jeg
     * å implementere en måte å starte spillet på nytt. Musikken begynner på nytt da, så det er 
     * jo noe :)
     */
    private void checkGameEndConditions() {
        if (humanPlayer.hasLost()) {
            JOptionPane.showMessageDialog(gameView, "You lost!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
            gameView.getStartButton().setEnabled(true);
            gameStarted = false;

            musicPlayer.stop();
        } else if (aiPlayer.hasLost()) {
            JOptionPane.showMessageDialog(gameView, "You won!", "Game Over", JOptionPane.INFORMATION_MESSAGE);
            gameView.getStartButton().setEnabled(true);
            gameStarted = false;

            musicPlayer.stop();
        }
    }

    /**
     * En privat klasse som håndterer klikk på celler i AI-spillerens brett. 
     * Når en celle klikkes på, kjøres HumanPlayer sin handleShot metode, og
     * trigger så AI-spillerens makeMove metode.
     */
    private class CellClickListener extends MouseAdapter {
        private final int row;
        private final int col;
    
        public CellClickListener(int row, int col) {
            this.row = row;
            this.col = col;
        }
    
        @Override
        public void mousePressed(MouseEvent e) {
            if (!gameStarted) {
                return;
            }
    
            humanPlayer.handleShot(aiPlayer, row, col);
            gameView.getHumanBoardView().refresh();
            gameView.getAIBoardView().refresh();
            
            // Lager en SwingWorker som hånterer AI-spillerens tur uten å blokkere Swing tråden.
            SwingWorker<Void, Void> aiWorker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    try {
                        // Legger til en forsinkelse på 0.8 sekunder for å gi illusjonen av at "AI tenker".
                        Thread.sleep(400);
                    } catch (InterruptedException ex) {
                    }
    
                    aiPlayer.makeMove(humanPlayer);
                    return null;
                }
    
                @Override
                protected void done() {
                    // Refresh both boards on the Swing thread and check for "game over"
                    gameView.getHumanBoardView().refresh();
                    gameView.getAIBoardView().refresh();
                    checkGameEndConditions();
                }
            };
            aiWorker.execute();
        }
    }

    // Getter for GameView, brukes av HomeController i den private StartGameListener 
    // klassen.
    public GameView getGameView() {
        return gameView;
    }
}




