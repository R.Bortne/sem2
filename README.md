# BattleShip

BattleShip er et spill som spilles både på brett, papir og på datamaskiner og spillkonsoller. De fleste har antageligvis enten hørt om eller spilt spillet
på et eller annet tidspunkt i sitt liv. For de som ikke aner hva spillet går ut på sp er det to spillere som plasserer diverse slagskip på et brett eller i et
rutenett, for så å gjette hverandres koordinater og skyte på motstanderens skip.

Dette prosjektet er en Java-versjon av BattleShip spillet. Her spiller en spiller mot en datamaskin(AI). Spillet bruker i all hovedsak Swing for det
grafiske brukergrensesnittet (UI).

## Spillregler

1. Hver spiller har et brett bestående av 10x10 ruter eller celler. 
2. Spillerne plasserer sine skip på brettet. I denne utgaven blir skipene plassert automatisk for både spilleren og datamaskinen.
3. Spillerene tar turer å skyte på hverandre ved å gjette på koordinater de tror motstanderens skip befinner seg i. Hvis skuddet treffer et skip markeres ruten som truffet (med rødfarge), om spilleren bommer markeres ruten med "bom", eller i denne utgaven en hvitfarge. Om alle cellene tilhørende et skip er truffet blir det markert som "sunket" eller med mørkerød farge i dette spillet.
4. Spillet fortsetter til en av spillerne har truffet alle rutene som inneholder motstandernes slagskip.

## Hvordan kjøre spillet

For å kjøre denne utgaven av spillet må det åpnes i en Java-kompatibel IDE (Eclipse, IntelliJ eller Visual Studio Code for eksempel).
Når mappen med spillet er åpnet i en kompatibel IDE, navigerer du til "Main" klassen og trykker "Run". Spillet åpner da et eget vindu hvor du kan 
se på instruksjoner om så ønskelig. Når du er klar for å spille trykker du "Start Game". Du vil da få opp et nytt vindu med selve slagbrettet.
Trykk så "BATTLE" knappen, og velg celler på mostanderens brett du vil skyte på. Du skyter med museklikk.

## Bugs og andre ting en bør være oppmerksom på

1. Spillet lagger etter tastetrykk om en bruker G-SYNC i Windows. Det anbefales at G-SYNC skrus av. 
2. Spillet skalerer ikke skikkelig i denne versjonen, og det er derfor anbefalt å la spill-vinduet være i sin opprinnelige størrelse. Om en er hellbent på å utvide eller minimere bør en passe på aspekt-ratioen.
3. Om en spiller trykker raskt på AI-brettet vil AI henge noe etter i sin skyting. Dette pga. 0.8 sekunders delay lagt inn for AI. Det anbefales å ikke skyte før AI har skutt.
4. Det ble ikke nok tid til å implementere en god nok "reset" mulighet som hadde hentet inn nye brett og startet spillet på nytt. En må derfor slå av spillet helt og restarte for å kunne spille et nytt spill i denne versjonen.

## Lenke til demonstrasjon av spillet på YouTube:

https://youtu.be/joy75Ms_QJk

## Ressurser som er brukt i prosjektet

Alle ressurser brukt i prosjektet er public domain, og er sånn sett ikke under copyright. 

Modeller av skip: https://opengameart.org/content/sea-warfare-set-ships-and-more

Musikk: http://vgmusic.com/

Home-vindu bilde: Laget av DALL-E https://openai.com/product/dall-e-2